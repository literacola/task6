//reflector mapping: http://www.codesandciphers.org.uk/enigma/example1.htm
import static settings.RotorSettings.*;

public class Reflector {

	private Pin[] pins = new Pin[KEYBOARD_SIZE];
	private Rotor last;
	private int timesRotated;
	
	public Reflector(char[] map, Rotor last) {
		this.timesRotated = 0;
		this.last = last;
		for (int i = 0; i < KEYBOARD_SIZE; i++) {
			char pinPosition = (char)(i+'A');
			pins[i] = new Pin(pinPosition);
		}
		for (int i = 0; i < KEYBOARD_SIZE; i++) {
			pins[i].setLinksToPin(pins[map[i]-'A']);
		}
	}
	
	public int getTimesRotated() {
		return this.timesRotated;
	}
	
	private void incrementTimesRotated() {
		this.timesRotated = (this.timesRotated + 1) % KEYBOARD_SIZE;
	}
	
	public void rotate() {
		this.incrementTimesRotated();
		rotateArray(this.pins);
	}
	
	private Pin[] rotateArray(Pin[] pins) {
		Pin temp = pins[0];
		for (int i = 0; i < KEYBOARD_SIZE-1; i++) {
			pins[i] = pins[i+1];
			pins[i].incrementDistFromTop();
		}
		pins[KEYBOARD_SIZE-1] = temp;
		pins[KEYBOARD_SIZE-1].incrementDistFromTop();
		return pins;
	}
	
	public char activatePin(int position) {
		int reflectedPin = pins[position].getLinksToPin().getDistFromTop();
		return last.activatePin(reflectedPin, 'b');
	}
	
}
