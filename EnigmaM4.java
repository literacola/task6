import static settings.RotorSettings.*;

public class EnigmaM4 implements EnigmaMachine {
	
	private Rotor[] rotors;	//rotors are set out from slowest to fastest, eg rotors[0]=slow, rotors[numRotors-1]=fast.
	private Reflector reflector;
	
	private int numRotors;

	public EnigmaM4(String walzenlage) {
		
		this.numRotors = walzenlage.length()-1;
		this.rotors = new Rotor[this.numberOfSettableWheels()];
		for (int i = this.numRotors-1; i >= 0; i--) {
			int rotorType = (int)walzenlage.charAt(i)-'0'-1;
			rotors[i] = new Rotor(rotorTypes[rotorType], turnoverPositions[rotorType][0], turnoverPositions[rotorType][1]);
		}
		
		this.reflector = new Reflector(reflectorTypes[(int)walzenlage.charAt(numRotors)-'A'], rotors[0]);
		
		rotors[numRotors-1].connectRotors(rotors[numRotors-2]);
		for (int i = numRotors-2; i > 0; i--) {
			rotors[i].connectRotors(rotors[i-1], rotors[i+1]);
		}
		rotors[0].connectRotors(reflector, rotors[1]);

	}
	
	
	   public int numberOfSettableWheels() {
		   return this.numRotors;
		   
	   }
	   
	 
	  public void setIndicators (String setting) {
		  // first set all rotors to default position
		  // need to do this from the Front rotor the the Back rotor, so that
		  // any stepping doesn't mess it up.
		  //FIXME: don't need to do this anymore!!
		  for (int i = (this.numberOfSettableWheels()-1); i >= 0; i--) {
			  while (rotors[i].getTimesRotated() != 0) {
				  rotors[i].rotate();
			  }
			  
			  int numRotations=0;
			  numRotations = setting.charAt(i) - 'A';
			   for(int j=0; j<numRotations; j++) {
				   rotors[i].rotate();
			   }
		  }
		  while (reflector.getTimesRotated() != 0) {
			  reflector.rotate();
		  }
		  int numRotations = setting.charAt(this.numberOfSettableWheels())-'A';
		  for (int i = 0; i < numRotations; i++) {
			  reflector.rotate();
		  }
	   }

	  
	  public String getCurrentIndicators () {
		  String currentIndicators = "";
		  for (int i = 0; i < numberOfSettableWheels(); i++) {
			  int numRotations = rotors[i].getTimesRotated();
			  String correspondingLetter = String.valueOf((char)(numRotations + 'A'));
			  currentIndicators = currentIndicators.concat(correspondingLetter);
		  }
		  int numRotations = reflector.getTimesRotated();
		  String correspondingLetter = String.valueOf((char)(numRotations + 'A'));
		  currentIndicators = currentIndicators.concat(correspondingLetter);
		  return currentIndicators;
	  }
	
	  private void rotateWithDoubleStepping() {
		  boolean prevRotatedAlready = false;
		  for (int j = 0; j < this.numberOfSettableWheels(); j++) {
			  if (rotors[j].isNotchInPosition()) {
				  rotors[j].rotate();
				  if (j != 0 && !prevRotatedAlready) {
					  rotors[j-1].rotate();
				  } else if (j == 0) {
					  reflector.rotate();
				  }
				  prevRotatedAlready = true;
			  } else {
				  prevRotatedAlready = false;
			  }
		  }
		  if (!prevRotatedAlready) {
			  rotors[this.numberOfSettableWheels()-1].rotate();
		  }
	  }
	  
	  public String encipher (String plainText) {
		  String encryptedString = "";
		  for (int i = 0; i < plainText.length(); i++) {
			  this.rotateWithDoubleStepping();
			  String encryptedLetter = String.valueOf((char)rotors[numRotors-1].activatePin(plainText.charAt(i) - 'A', 'f'));
			  encryptedString = encryptedString.concat(encryptedLetter);
//			  System.out.println(plainText.charAt(i) + " -> " + encryptedLetter);
		  }
		  System.out.println(plainText + " -> " + encryptedString);
		  return encryptedString;
	  }
	  
}
