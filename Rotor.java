import static settings.RotorSettings.*;

public class Rotor {
	private Rotor nextRotor;
	private Rotor previousRotor;
	private Reflector reflector;
	public Pin[] pinsFront = new Pin[KEYBOARD_SIZE];
	public Pin[] pinsBack = new Pin[KEYBOARD_SIZE];
	private int timesRotated;
	private int turnoverPosition1;
	private int turnoverPosition2;
	
	public Rotor(char[] map, int turnoverPosition1, int turnoverPosition2) {
		this.turnoverPosition1 = turnoverPosition1;
		this.turnoverPosition2 = turnoverPosition2;
		this.timesRotated = 0;
		for (int i = 0; i < KEYBOARD_SIZE; i++) {
			char pinPosition = (char)(i+'A');
			// map the positions in the front pins to where they connect to on the back pins
			pinsFront[i] = new Pin(pinPosition);
			pinsBack[i] = new Pin(pinPosition);
			
		}
		for (int i = 0; i < KEYBOARD_SIZE; i++) {
			pinsFront[i].setLinksToPin(pinsBack[map[i]-'A']);
			// now map the positions on the back pins to where they connect to on the front pins.
			char transposedMapChar = 0;
			char pinPosition = (char)(i+'A');
			for (int j = 0; j < KEYBOARD_SIZE; j++) {
				if (map[j] == pinPosition) {
					transposedMapChar = (char)(j + 'A');
				}
			}
			pinsBack[i].setLinksToPin(pinsFront[transposedMapChar-'A']);
		}

	}
	
	public int getTurnoverPosition1() {
		return this.turnoverPosition1;
	}
	
	public int getTurnoverPosition2() {
		return this.turnoverPosition2;
	}
	
	public int getTimesRotated() {
		return this.timesRotated;
	}
	
	public void incrementTimesRotated() {
		this.timesRotated = (this.timesRotated + 1) % KEYBOARD_SIZE;
	}
	
	// this is used for when the rotor is between 2 other rotors
	public void connectRotors(Rotor next, Rotor previous) {
		this.nextRotor = next;
		this.previousRotor = previous;
	}
	
	// this is used for the first rotor - it connects to the next rotor
	public void connectRotors(Rotor next) {
		this.nextRotor = next;
	}
	
	// this is used for the last rotor - it connects from a rotor to the reflector.
	public void connectRotors(Reflector reflector, Rotor previous) {
		this.reflector = reflector;
		this.previousRotor = previous;
	}
	
	public void rotate() {
//		System.out.println("ROTATE");
// 		If this rotor has reached its TurnoverPosition, then it should rotate the next rotor along.
//		if (this.isNotchInPosition()) {
//			if (!this.isLastRotor()) {
//				this.nextRotor.rotate();
//			}
//		}
		
		this.incrementTimesRotated();
		// rotate the arrays of pins
		pinsFront = rotateArray(pinsFront);
		pinsBack = rotateArray(pinsBack);
		
		
	}
	
	public boolean isNotchInPosition() {
		return (this.getTimesRotated() == this.getTurnoverPosition1() || this.getTimesRotated() == this.getTurnoverPosition2());
	}
	
	private Pin[] rotateArray(Pin[] pins) {
		Pin temp = pins[0];
		for (int i = 0; i < KEYBOARD_SIZE-1; i++) {
			pins[i] = pins[i+1];
			pins[i].incrementDistFromTop();
		}
		pins[KEYBOARD_SIZE-1] = temp;
		pins[KEYBOARD_SIZE-1].incrementDistFromTop();
		return pins;
	}
	
	private boolean isFirstRotor() {
		return (this.previousRotor == null);
	}
	
	private boolean isLastRotor() {
		return (this.nextRotor == null);
	}
	
	public char activatePin(int position, char direction) {
		char returnValue = 0;
		
		if (direction == 'f') {
			if (!this.isLastRotor()) {
				returnValue = nextRotor.activatePin(this.pinsFront[position].getLinksToPin().getDistFromTop(), direction);
			} else {
				returnValue = reflector.activatePin(this.pinsFront[position].getLinksToPin().getDistFromTop());
			}
			
		} else if(direction == 'b') {

			if (!this.isFirstRotor()) {
				returnValue = previousRotor.activatePin(this.pinsBack[position].getLinksToPin().getDistFromTop(), direction);
			} else {
				returnValue = (char)(this.pinsBack[position].getLinksToPin().getDistFromTop() + 'A');
			}
		}
		
		return returnValue;
	}
	
	
	
	public void displayPin(int num) {
		System.out.println(this.pinsFront[num].getLetter() + " : " + this.pinsFront[num].getLinksToPin());
	}
	
	
	
	public void displayMapping() {
		System.out.println("-----------------");
		for(int i=0; i<KEYBOARD_SIZE; i++) {
			System.out.println(pinsFront[i].getLetter() + " : " + pinsFront[i].getLinksToPin().getLetter());
		}
		System.out.println("-----------------");
	}
}
