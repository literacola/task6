
public class testEnigma {

	
	public static void main(String[] args) {
		
		System.out.println("Testing enigma...");
		Enigma enigma = new Enigma("123B");
		enigma.setIndicators("AAA");
		assert(enigma.getCurrentIndicators().equals("AAA"));
		enigma.setIndicators("HEY");
		assert(enigma.getCurrentIndicators().equals("HEY"));
		
		String encrypted;
		enigma.setIndicators("AAA");
		assert(enigma.getCurrentIndicators().equals("AAA"));
		
		
		encrypted = enigma.encipher("NIFBOELFWAXXJNZZSAPSLQEAKZRGQEEJUJUGAANJJTPJCBGWEKFZXOIWZTDQUILXIACVBQZBWYIFLNM");
		System.out.println(encrypted);
		
		System.out.println("All tests passed!");
		
		
		
	}
	
}
