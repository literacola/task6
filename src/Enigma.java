
public class Enigma implements EnigmaMachine {
	
	public static final int KEYBOARD_SIZE = 26;
	
	//http://en.wikipedia.org/wiki/Enigma_rotor_details
	
	private char[] rotorI = {'E','K','M','F','L','G','D','Q','V','Z','N','T','O','W','Y','H','X','U','S','P','A','I','B','R','C','J'};
	private char[] rotorII = {'A','J','D','K','S','I','R','U','X','B','L','H','W','T','M','C','Q','G','Z','N','P','Y','F','V','O','E'};
	private char[] rotorIII = {'B','D','F','H','J','L','C','P','R','T','X','V','Z','N','Y','E','I','W','G','A','K','M','U','S','Q','O'};
	private char[] rotorIV = {'E','S','O','V','P','Z','J','A','Y','Q','U','I','R','H','X','L','N','F','T','G','K','D','C','M','W','B'};
	private char[] rotorV = {'V','Z','B','R','G','I','T','Y','U','P','S','D','N','H','L','X','A','W','M','J','Q','O','F','E','C','K'};
	private char[] rotorVI = {'J','P','G','V','O','U','M','F','Y','Q','B','E','N','H','Z','R','D','K','A','S','X','L','I','C','T','W'};
	private char[] rotorVII = {'N','Z','J','H','G','R','C','X','M','Y','S','W','B','O','U','F','A','I','V','L','P','E','K','Q','D','T'};
	private char[] rotorVIII = {'F','K','Q','H','T','L','X','O','C','B','J','S','P','D','Z','R','A','M','E','W','N','I','U','Y','G','V'};
	
	private int[] turnoverPositions = {17,5,22,10,0,0,0,0};	//rotors 6/7/8 also need to implement N turnover position
	
	private char[] reflectorMapA = {'E','J','M','Z','A','L','Y','X','V','B','W','F','C','R','Q','U','O','N','T','S','P','I','K','H','G','D'};
	private char[] reflectorMapB = {'Y','R','U','H','Q','S','L','D','P','X','N','G','O','K','M','I','E','B','F','Z','C','W','V','J','A','T'};
	private char[] reflectorMapC = {'F','V','P','J','I','A','O','Y','E','D','R','Z','X','W','G','C','T','K','U','Q','S','B','N','M','H','L'};
	
	private char[][] rotorTypes = {rotorI, rotorII, rotorIII, rotorIV, rotorV, rotorVI, rotorVII, rotorVIII};
	private char[][] reflectorTypes = {reflectorMapA,reflectorMapB,reflectorMapC};  
	
	public Rotor Front;
	public Rotor Middle;
	public Rotor Back;
	private Rotor[] rotors = new Rotor[this.numberOfSettableWheels()];

	public Enigma(String walzenlage) {
		
		int rotorType = (int)walzenlage.charAt(2)-'0'-1;
		Front = new Rotor(rotorTypes[rotorType], turnoverPositions[rotorType]);
		rotorType = (int)walzenlage.charAt(1)-'0'-1;
		Middle = new Rotor(rotorTypes[rotorType], turnoverPositions[rotorType]);
		rotorType = (int)walzenlage.charAt(0)-'0'-1;
		Back = new Rotor(rotorTypes[rotorType], turnoverPositions[rotorType]);
		
		rotors[0] = Back;
		rotors[1] = Middle;
		rotors[2] = Front;
		
		Reflector reflector = new Reflector(reflectorTypes[(int)walzenlage.charAt(3)-'A'], Back);
		
		Front.connectRotors(Middle);
		Middle.connectRotors(Back, Front);
		Back.connectRotors(reflector, Middle);

	}
	
	
	   public int numberOfSettableWheels() {
		   return(3); //fix pending tests
		   
	   }
	   
	 
	  public void setIndicators (String setting) {
		  // first set all rotors to default position
		  // need to do this from the Front rotor the the Back rotor, so that
		  // any stepping doesn't mess it up.
		  for (int i = (this.numberOfSettableWheels()-1); i >= 0; i--) {
			  while (rotors[i].getTimesRotated() != 0) {
				  rotors[i].rotate();
			  }
			  
			  int numRotations=0;
			  numRotations = setting.charAt(i) - 'A';
			   for(int j=0; j<numRotations; j++) {
				   rotors[i].rotate();
			   }
		  }
	   }

	  
	  public String getCurrentIndicators () {
		  String currentIndicators = "";
		  for (int i = 0; i < numberOfSettableWheels(); i++) {
			  int numRotations = rotors[i].getTimesRotated();
			  String correspondingLetter = String.valueOf((char)(numRotations + 'A'));
			  currentIndicators = currentIndicators.concat(correspondingLetter);
		  }
		  return currentIndicators;
	  }
	
	  
	  public String encipher (String plainText) {
		  String encryptedString = "";
		  for (int i = 0; i < plainText.length(); i++) {
			  String encryptedLetter = String.valueOf((char)Front.activatePin(plainText.charAt(i) - 'A', 'f'));
			  encryptedString = encryptedString.concat(encryptedLetter);
//			  System.out.println(plainText.charAt(i) + " -> " + encryptedLetter);
		  }
		  System.out.println(plainText + " -> " + encryptedString);
		  return encryptedString;
	  }
	  
}
