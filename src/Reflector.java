//reflector mapping: http://www.codesandciphers.org.uk/enigma/example1.htm

public class Reflector {

	private Pin[] pins = new Pin[Enigma.KEYBOARD_SIZE];
	private Rotor last;
	
	public Reflector(char[] map, Rotor last) {
		this.last = last;
		for (int i = 0; i < Enigma.KEYBOARD_SIZE; i++) {
			char pinPosition = (char)(i+'A');
			pins[i] = new Pin(pinPosition);
		}
		for (int i = 0; i < Enigma.KEYBOARD_SIZE; i++) {
			pins[i].setLinksToPin(pins[map[i]-'A']);
		}
	}
	
	public char activatePin(int nextPin) {
		int reflectedPin = pins[nextPin].getLinksToPin().getDistFromTop();
		return last.activatePin(reflectedPin, 'b');
	}
	
}
