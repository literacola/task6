
public class Pin {
	/* A pin has:
	 * a link to the pin on the other side of the rotor
	 * 
	 * its current absolute position (ie, if the rotor is set
	 * to the key A, and this pin is on A, then its absolute
	 * position is 0. If the rotor is turned so that B is now
	 * the 'key', the pin's absolute position is now 1). the
	 * reason for this is so that we know which pin on the
	 * NEXT rotor this one will be sending the current to.
	 * 
	 * what letter it lies on.
	 */
	
	private char letter;
	private int distFromTop;
	private Pin linksToPin;
	
	public Pin(char letter) {
		this.letter = letter;
		this.distFromTop = letter - 'A';
	}
	
	public void incrementDistFromTop() {
		this.distFromTop--;
		if (this.distFromTop < 0) {
			this.distFromTop += Enigma.KEYBOARD_SIZE;
		}
	}
	
	public void setLinksToPin(Pin p) {
		this.linksToPin = p;
	}

	public int getDistFromTop() {
		return distFromTop;
	}

	public char getLetter() {
		return letter;
	}

	public Pin getLinksToPin() {
		return linksToPin;
	}
	
	
}
