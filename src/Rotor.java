public class Rotor {
	private Rotor nextRotor;
	private Rotor previousRotor;
	private Reflector reflector;
	public Pin[] pinsFront = new Pin[Enigma.KEYBOARD_SIZE];
	public Pin[] pinsBack = new Pin[Enigma.KEYBOARD_SIZE];
	private int timesRotated;
	private int turnoverPosition;
	private boolean doubleStepNextRotation = false;
	
	public Rotor(char[] map, int turnoverPosition) {
		this.turnoverPosition = turnoverPosition;
		timesRotated = 0;
		for (int i = 0; i < Enigma.KEYBOARD_SIZE; i++) {
			char pinPosition = (char)(i+'A');
			// map the positions in the front pins to where they connect to on the back pins
			pinsFront[i] = new Pin(pinPosition);
			pinsBack[i] = new Pin(pinPosition);
			
		}
		for (int i = 0; i < Enigma.KEYBOARD_SIZE; i++) {
			pinsFront[i].setLinksToPin(pinsBack[map[i]-'A']);
			// now map the positions on the back pins to where they connect to on the front pins.
			char transposedMapChar = 0;
			char pinPosition = (char)(i+'A');
			for (int j = 0; j < Enigma.KEYBOARD_SIZE; j++) {
				if (map[j] == pinPosition) {
					transposedMapChar = (char)(j + 'A');
				}
			}
			pinsBack[i].setLinksToPin(pinsFront[transposedMapChar-'A']);
		}

	}
	
	public int getTurnoverPosition() {
		return this.turnoverPosition;
	}
	
	public int getTimesRotated() {
		return this.timesRotated;
	}
	
	public void incrementTimesRotated() {
		this.timesRotated = (this.timesRotated + 1) % Enigma.KEYBOARD_SIZE;
	}
	
	// this is used for when the rotor is between 2 other rotors
	public void connectRotors(Rotor next, Rotor previous) {
		this.nextRotor = next;
		this.previousRotor = previous;
	}
	
	// this is used for the first rotor - it connects to the next rotor
	public void connectRotors(Rotor next) {
		this.nextRotor = next;
	}
	
	// this is used for the last rotor - it connects from a rotor to the reflector.
	public void connectRotors(Reflector reflector, Rotor previous) {
		this.reflector = reflector;
		this.previousRotor = previous;
	}
	
	public void rotate() {
//		System.out.println("ROTATE");
		this.incrementTimesRotated();
		// rotate the arrays of pins
		pinsFront = rotateArray(pinsFront);
		pinsBack = rotateArray(pinsBack);
		
		// If this rotor has reached its TurnoverPosition, then it should rotate the next rotor along.
		if (this.getTimesRotated() == this.getTurnoverPosition()) {
			if (!this.isLastRotor()) {
				this.nextRotor.rotate();
				// DOUBLE STEPPING:
				// If the next rotor is 1 rotation away from its TurnoverPosition AND it isn't the last rotor, 
				// then we need to rotate it on the NEXT rotation of this rotor.
				if ((this.nextRotor.getTimesRotated()+1) % Enigma.KEYBOARD_SIZE == this.nextRotor.getTurnoverPosition() && !this.nextRotor.isLastRotor()) {
					this.doubleStepNextRotation = true;
				}
			}
		} else if (this.doubleStepNextRotation == true) {
			this.nextRotor.rotate();
			this.doubleStepNextRotation = false;
		}
	}
	
	private Pin[] rotateArray(Pin[] pins) {
		Pin temp = pins[0];
		for (int i = 0; i < Enigma.KEYBOARD_SIZE-1; i++) {
			pins[i] = pins[i+1];
			pins[i].incrementDistFromTop();
		}
		pins[Enigma.KEYBOARD_SIZE-1] = temp;
		pins[Enigma.KEYBOARD_SIZE-1].incrementDistFromTop();
		return pins;
	}
	
	private boolean isFirstRotor() {
		return (this.previousRotor == null);
	}
	
	private boolean isLastRotor() {
		return (this.nextRotor == null);
	}
	
	public char activatePin(int position, char direction) {
		char returnValue = 0;
		
		if (direction == 'f') {
			if (this.isFirstRotor()) {
				this.rotate();
			}
			if (!this.isLastRotor()) {
				returnValue = nextRotor.activatePin(this.pinsFront[position].getLinksToPin().getDistFromTop(), direction);
			} else {
				returnValue = reflector.activatePin(this.pinsFront[position].getLinksToPin().getDistFromTop());
			}
			
		} else if(direction == 'b') {

			if (!this.isFirstRotor()) {
				returnValue = previousRotor.activatePin(this.pinsBack[position].getLinksToPin().getDistFromTop(), direction);
			} else {
				returnValue = (char)(this.pinsBack[position].getLinksToPin().getDistFromTop() + 'A');
			}
		}
		
		return returnValue;
	}
	
	
	
	public void displayPin(int num) {
		System.out.println(this.pinsFront[num].getLetter() + " : " + this.pinsFront[num].getLinksToPin());
	}
	
	
	
	public void displayMapping() {
		System.out.println("-----------------");
		for(int i=0; i<Enigma.KEYBOARD_SIZE; i++) {
			System.out.println(pinsFront[i].getLetter() + " : " + pinsFront[i].getLinksToPin().getLetter());
		}
		System.out.println("-----------------");
	}
}
