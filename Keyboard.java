import java.util.*;

public class Keyboard {
	public static final int KEYBOARD_SIZE = 26;
	
	public static void main(String[] args) {
		char[] keyPressed;
		Scanner sc = new Scanner(System.in);
		keyPressed = sc.next().toCharArray();
		for (char c : keyPressed) {
			System.out.println(c);
		}
	}
	
}
